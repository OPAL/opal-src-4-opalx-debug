#pragma once

#include <memory>

class _Astra1DDynamic;
using Astra1DDynamic = std::shared_ptr<_Astra1DDynamic>;

class _Astra1DDynamic_fast;
using Astra1DDynamic_fast = std::shared_ptr<_Astra1DDynamic_fast>;

class _Astra1DElectroStatic;
using Astra1DElectroStatic = std::shared_ptr<_Astra1DElectroStatic>;

class _Astra1DElectroStatic_fast;
using Astra1DElectroStatic_fast = std::shared_ptr<_Astra1DElectroStatic_fast>;

class _Astra1DMagnetoStatic;
using Astra1DMagnetoStatic = std::shared_ptr<_Astra1DMagnetoStatic>;

class _Astra1DMagnetoStatic_fast;
using Astra1DMagnetoStatic_fast = std::shared_ptr<_Astra1DMagnetoStatic_fast>;

class _Fieldmap;
using Fieldmap = std::shared_ptr<_Fieldmap>;

class _FM1DDynamic;
using FM1DDynamic = std::shared_ptr<_FM1DDynamic>;

class _FM1DDynamic_fast;
using FM1DDynamic_fast = std::shared_ptr<_FM1DDynamic_fast>;

class _FM1DElectroStatic;
using FM1DElectroStatic = std::shared_ptr<_FM1DElectroStatic>;

class _FM1DElectroStatic_fast;
using FM1DElectroStatic_fast = std::shared_ptr<_FM1DElectroStatic_fast>;

class _FM1DMagnetoStatic;
using FM1DMagnetoStatic = std::shared_ptr<_FM1DMagnetoStatic>;

class _FM1DMagnetoStatic_fast;
using FM1DMagnetoStatic_fast = std::shared_ptr<_FM1DMagnetoStatic_fast>;

class _FM1DProfile1;
using FM1DProfile1 = std::shared_ptr<_FM1DProfile1>;

class _FM1DProfile2;
using FM1DProfile2 = std::shared_ptr<_FM1DProfile2>;

class _FM2DDynamic;
using FM2DDynamic = std::shared_ptr<_FM2DDynamic>;

class _FM2DElectroStatic;
using FM2DElectroStatic = std::shared_ptr<_FM2DElectroStatic>;

class _FM2DMagnetoStatic;
using FM2DMagnetoStatic = std::shared_ptr<_FM2DMagnetoStatic>;

class _FM3DDynamic;
using FM3DDynamic = std::shared_ptr<_FM3DDynamic>;

class _FM3DH5Block;
using FM3DH5Block = std::shared_ptr<_FM3DH5Block>;

class _FM3DH5Block_nonscale;
using FM3DH5Block_nonscale = std::shared_ptr<_FM3DH5Block_nonscale>;

class _FM3DMagnetoStatic;
using FM3DMagnetoStatic = std::shared_ptr<_FM3DMagnetoStatic>;

class _FM3DMagnetoStaticExtended;
using FM3DMagnetoStaticExtended = std::shared_ptr<_FM3DMagnetoStaticExtended>;

class _FM3DMagnetoStaticH5Block;
using FM3DMagnetoStaticH5Block = std::shared_ptr<_FM3DMagnetoStaticH5Block>;