set (_SRCS
    AsymmetricEngeTest.cpp
    EndFieldModelTest.cpp
    EngeTest.cpp
    TanhTest.cpp
)

add_sources(${_SRCS})
