add_subdirectory (AbsBeamline)
add_subdirectory (Algorithms)
add_subdirectory (Fields)
add_subdirectory (Solvers)
add_subdirectory (Structure)
add_subdirectory (Utilities)

set (TEST_SRCS_LOCAL ${TEST_SRCS_LOCAL} PARENT_SCOPE)